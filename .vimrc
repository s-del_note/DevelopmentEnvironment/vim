set encoding=utf-8
set fileencodings=utf-8,sjis,cp932
set title
set visualbell t_vb=
set number
set nowrap
set nofixeol

"ruler の設定
set colorcolumn=72,79,99
highlight ColorColumn ctermbg=234

"空白文字の表示設定
set listchars=space:.,tab:>\ ,trail:_
highlight SpecialKey ctermfg=236
set list

"ステータスラインの設定
set laststatus=2
set statusline=%f%m%r%h%w
set statusline+=%=
set statusline+=\ [Row:\ %l/%L]
set statusline+=\ [Col:\ %v]
set statusline+=\ [indent:\ %{&tabstop}]
set statusline+=\ [fenc:\ %{&fileencoding}]
set statusline+=\ [ff:\ %{&fileformat}]

"インデント設定
set cindent
set noexpandtab
set tabstop=3
augroup fileTypeIndent
    autocmd!
    autocmd BufNewFile,BufRead *.json setlocal expandtab tabstop=2 softtabstop=2 shiftwidth=2
    autocmd BufNewFile,BufRead *.txt  setlocal expandtab tabstop=4 softtabstop=4 shiftwidth=4
    autocmd BufNewFile,BufRead *.xml  setlocal expandtab tabstop=4 softtabstop=4 shiftwidth=4
    autocmd BufNewFile,BufRead *.yaml setlocal expandtab tabstop=4 softtabstop=4 shiftwidth=4
    autocmd BufNewFile,BufRead *.yml  setlocal expandtab tabstop=4 softtabstop=4 shiftwidth=4
    autocmd BufNewFile,BufRead *rc    setlocal expandtab tabstop=4 softtabstop=4 shiftwidth=4
augroup END

"検索を very magic に変更
nnoremap / /\v

"K キーでカーソル位置の単語の :help を開く
augroup set_kp_help
    autocmd FileType vim,help setlocal keywordprg=:help
augroup END
